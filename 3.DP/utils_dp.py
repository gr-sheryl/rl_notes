#!/usr/bin/python
# -*- coding: utf-8 -*-


# 得到概率值
def get_prob(P, s, a, s1):
    return P(s, a, s1)


# 得到奖励
def get_reward(R, s, a):
    return R(s, a)


#显示状态价值
def display_V(V):
    for i in range(16):
        print('{0:>6.2f}'.format(V[i]), end = " ")
        if (i+1) % 4 == 0:
            print("")
    print()


# 设置状态价值
def set_value(V, s, v):
    V[s] = v


# 得到状态价值
def get_value(V, s):
    return V[s]


# 得到策略
def get_pi(Pi, s, a, MDP=None, V=None):
    return Pi(MDP, V, s, a)
