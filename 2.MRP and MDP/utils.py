#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
target:
    将参数用"_"连接，参数本身可能是list或者tuple形式
args:
    args 形参
return：
    用"_"连接的字符串
"""


def str_key(*args):
    new_arg = []
    for arg in args:
        if type(arg) in [tuple, list]:
            new_arg += [str(i) for i in arg]
        else:
            new_arg.append(str(arg))
    return "_".join(new_arg)

#设置字典
def set_dict(target_dict, value, *args):
    target_dict[str_key(*args)] = value

#设置概率值
def set_prob(P, s, a, s1, p=1.0):
    set_dict(P, p, s, a, s1)

#得到概率值
def get_prob(P, s, a, s1):
    return P.get(str_key(s, a, s1), 0)

#设置奖励
def set_reward(R, s, a, r):
    set_dict(R, r, s, a)

#得到奖励
def get_reward(R, s, a):
    return R.get(str_key(s, a), 0)

#显示字典内容
def display_dict(target_dict):
    for key in target_dict.keys():
        print("{}:  {:.2f}".format(key, target_dict[key]))

#设置状态价值
def set_value(V, s, v):
    set_dict(V, v, s)

#得到状态价值
def get_value(V, s):
    return V.get(str_key(s), 0)

#设置策略
def set_pi(Pi, s, a, p=0.5):
    set_dict(Pi, p, s, a)

#得到策略
def get_pi(Pi, s, a):
    return Pi.get(str_key(s, a), 0)
